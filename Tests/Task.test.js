const { describe, it, before } = require('zunit');
const assert = require('assert');

const Database = require('../Infrastructure/Database/Connection/Array.js');
const DatabaseContext = require('../Infrastructure/Database/Context/Array/Task.js');
const TaskRepository = require('../Infrastructure/Repository/Task.js');
const TaskService = require('../Infrastructure/TaskService.js');
const TaskDTO = require('../Domain/TaskDTO.js');

let rightTask = {
  title: "Test title",
  description: "Just another task",
  created: +new Date()
};
let wrongTask = {
  title: null,
  description: "Just another task",
  deleted: +new Date()
};

let taskService;
let task;

describe('TaskService', () => {

  before(async () => {
    let database = new Database();
    let databaseConnection = await database.getConnection();
    let databaseContext = new DatabaseContext(databaseConnection);
    let taskRepository = new TaskRepository(databaseContext);
    taskService = new TaskService(taskRepository);
  });

  describe('create()', () => {

    it('creates a task with right values', async () => {
      task = await taskService.create(rightTask);

      assert.ok(task instanceof TaskDTO);
    });

    it('does not create a task without title', async () => {
      let task = taskService.create(wrongTask);

      await assert.rejects(task, {
        message: 'Can not create a task'
      });
    });

  });

  describe('getTask()', () => {

    it('gets a task', async () => {
      let taskFromService = await taskService.getTask(task.id);

      assert.strictEqual(task.id, taskFromService.id);
      assert.strictEqual(task.title, taskFromService.title);
      assert.strictEqual(task.description, taskFromService.description);
      assert.strictEqual(task.created, taskFromService.created);
      assert.strictEqual(task.deleted, taskFromService.deleted);
    });

    it('does not get a task', async () => {
      let task = taskService.getTask(-1);

      await assert.rejects(task, {
        message: 'Task does not exist'
      });
    });

  });

  describe('getTasks()', () => {

    it('gets tasks', async () => {
      let tasks = await taskService.getTasks();

      assert.ok(Array.isArray(tasks));
      assert.strictEqual(tasks.length, 1);
    });

    it('every task is DTO', async () => {
      let tasks = await taskService.getTasks();

      assert.ok(tasks.every(task => task instanceof TaskDTO));
    });

    it('does not get tasks after flush', async () => {
      await taskService.TaskRepository.db.flush();

      let tasks = taskService.getTasks();

      await assert.rejects(tasks, {
        message: 'No tasks'
      });
    });

  });

  describe('countTasks()', () => {

    it('returns zero after flush', async () => {
      await taskService.TaskRepository.db.flush();

      let count = await taskService.countTasks();

      assert.strictEqual(count, 0);
    });

    it('returns X after adding X tasks', async () => {
      let initialCount = Math.floor(Math.random() * 7) + 3;
      for (let i = 0; i < initialCount; i++) {
        await taskService.create(rightTask);
      }
      let count = await taskService.countTasks();

      assert.strictEqual(count, initialCount);
    })

  });

  describe('deleteTask()', () => {

    it('removes the task', async () => {
      let initialCount = await taskService.countTasks();
      let id = Math.min(1, initialCount - 2);
      let task = await taskService.getTask(id);
      await taskService.delete(task);
      let count = await taskService.countTasks();

      assert.strictEqual(initialCount, count + 1);
    });

    it('removes the defined task correctly', async () => {
      let tasks = await taskService.getTasks();
      let task = tasks[Math.floor(Math.random() * tasks.length)];
      let deletedTask = await taskService.delete(task);

      assert.strictEqual(task.id, deletedTask.id);
    })

  });


});
