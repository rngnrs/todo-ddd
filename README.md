## todo-ddd
##### *Simple Node.js application that tries to follow DDD*

#### Installation

##### Prerequisites
- Node.js with npm or npx

##### Installation steps
```
# Install using npm
npm i

# Install using yarn
yarn install
```

#### Usage
```
# Launch on LTS or "current" version
node index.js   # or `npm start` or `yarn start`

# Launch on non-LTS
npx -p node@lts -- node index.js
```