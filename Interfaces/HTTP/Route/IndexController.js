const MainController = require('../MainController.js');

class IndexController extends MainController {

  constructor(Router) {
    super(Router);

    Router.get('/', this.getIndex.bind(this));
  }

  getIndex(ctx) {
    let out = {
      message: "Simple Node.js application that tries to follow DDD",
      link: "/api/"
    };

    this.success(ctx, out);
  }

}

module.exports = IndexController;