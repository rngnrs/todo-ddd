const http = require('http');

class HTTP {

  constructor({ Framework, Router } = {}) {
    this.HttpServer = http.createServer();
    this.Framework = Framework;
    this.Router = Router;

    this.HttpServer.on('request', this.Framework.callback());
  }

  onListen(callback) {
    this.HttpServer.on('listening', callback);
  }

  async loadRoutes() {
    this.Routes = require('./Route/index.js');

    for (let j = 0; j < this.Routes.length; j++) {
      let Route = this.Routes[j];
      if (!Route) {
        continue;
      }
      Route = new Route(this.Router);
      this.Framework.use(Route.Router.routes());
      this.Framework.use(Route.Router.allowedMethods());
    }
  }

  async listenSocket(socket) {
    return this.HttpServer.listen(socket);
  }

  async listenPort(host, port) {
    return this.HttpServer.listen(port, host);
  }

}

module.exports = HTTP;
