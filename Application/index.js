const Koa = require('koa');
const KoaRouter = require('koa-router');

const HTTP = require('../Interfaces/HTTP/index.js');

class Application {

  constructor({ logger = console, config } = {}) {
    this.logger = logger;
    this.config = config;
  }

  async init() {
    this.logUnexpectedErrors(this.logger.error.bind(this));
    try {
      this.logger.log('TDD-driven development!!1 Please check `npm test`.');

      await this.initHTTP();
    } catch (e) {
      this.logger.error(e);
      process.exit(1);
    }
  }

  async initHTTP() {
    this.http = new HTTP({
      Framework: new Koa(),
      Router: KoaRouter()
    });
    let { listen, socket, host, port } = this.config.http;

    this.http.onListen(() => {
      this.logger.log(`\x1b[32mListening\x1b[0m http://${host}:${port}...`)
    });

    await this.http.loadRoutes();

    if (listen === 'socket') {
      return this.http.listenSocket(socket);
    }
    return this.http.listenPort(host, port);
  }

  logUnexpectedErrors(output) {
    process.on('uncaughtException', output);
    process.on('unhandledRejection', output);
    process.on('warning', output);
  }

}

module.exports = Application;
