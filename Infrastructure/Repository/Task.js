class TaskRepository {

  constructor(DatabaseContext) {
    this.db = DatabaseContext;
  }

  async store(Task) {
    return this.db.create(Task);
  }

  async readOneById(id) {
    return this.db.readOneById(id);
  }

  async readAll() {
    return this.db.readAll();
  }

  async count() {
    return this.db.count();
  }

  async removeById(id) {
    return this.db.deleteById(id);
  }

}

module.exports = TaskRepository;
