class ArrayConnection {

  constructor() {
    //this.createConnection();
  }

  async createConnection() {
    this.connection = [];
  }

  async getConnection() {
    if (!this.connection) {
      await this.createConnection();
    }
    return this.connection;
  }

}

module.exports = ArrayConnection;
