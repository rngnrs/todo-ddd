class TaskArrayDatabaseContext {

  constructor(connection) {
    this.connection = connection;
    this.id = 0;
  }

  async create(Task) {
    Task.id = ++this.id;
    this.connection.push(Task);
    return Task;
  }

  async readOneById(id) {
    return this.connection.filter(task => task.id === id)[0];
  }

  async readAll() {
    return this.connection;
  }

  async count() {
    return this.connection.length;
  }

  async deleteById(id) {
    let task = await this.readOneById(id);
    let index = this.connection.findIndex(t => t.id === task.id);
    return this.connection.splice(index, 1)[0];
  }

  async flush() {
    this.id = 0;
    this.connection = [];
    return true;
  }

}

module.exports = TaskArrayDatabaseContext;
