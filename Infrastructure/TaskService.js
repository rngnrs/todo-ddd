const TaskDTO = require('../Domain/TaskDTO.js');

class TaskService {

  constructor(TaskRepository) {
    this.TaskRepository = TaskRepository;
  }

  async create(data) {
    let task = TaskDTO.from(data);
    if (!task.title || !task.description) {
      throw new Error('Can not create a task');
    }
    return this.TaskRepository.store(task);
  }

  async getTask(id) {
    let task = await this.TaskRepository.readOneById(id);
    if (!task) {
      throw new Error('Task does not exist');
    }
    return TaskDTO.from(task);
  }

  async getTasks() {
    let tasks = await this.TaskRepository.readAll();
    if (!tasks || !tasks.length) {
      throw new Error('No tasks');
    }
    return tasks.map(task => TaskDTO.from(task));
  }

  async countTasks() {
    return this.TaskRepository.count();
  }

  async delete(dto) {
    return this.TaskRepository.removeById(dto.id);
  }

}

module.exports = TaskService;
