const path = require('path');
const os = require('os');

module.exports = {
  http: {
    listen: process.env.SOCKET ? 'socket' : 'port',

    socket: process.env.SOCKET || path.join(os.tmpdir(), 'todo.socket'),
    host: process.env.HOST || '0.0.0.0',
    port: process.env.PORT || 8080
  }
};
