const DTO = require('./DTO.js');

class TaskDTO extends DTO {

  constructor(data) {
    super(data);

    this.id = data.id;
    this.title = data.title;
    this.description = data.description;
    this.created = data.created;
    this.deleted = data.deleted;

  }

}

module.exports = TaskDTO;
