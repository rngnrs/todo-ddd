const Todo = require('./Application/index.js');
const config = require('./config.js');

(async () => {
  await new Todo({
    logger: console,
    config
  }).init();
})();
